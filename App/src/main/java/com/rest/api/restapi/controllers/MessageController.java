package com.rest.api.restapi.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.rest.api.restapi.domain.Message;
import com.rest.api.restapi.repositories.MessageRepository;
import com.rest.api.restapi.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@org.springframework.web.bind.annotation.RestController
@RequestMapping({"/api"})
public class MessageController
{
    private final MessageRepository msgRespository;
    private final EmailService emailService;
    private final CassandraTemplate cassandraTemplate;

    @Autowired
    public MessageController(MessageRepository msgRespository, EmailService emailService, CassandraTemplate cassandraTemplate)
    {
        this.msgRespository = msgRespository;
        this.emailService = emailService;
        this.cassandraTemplate = cassandraTemplate;
    }

    @RequestMapping(value="/message", method= RequestMethod.GET)
    public List<Message> listMessages(Model model) {
        return msgRespository.findAll();
    }

    @RequestMapping(value="/message", method=RequestMethod.POST, consumes={"application/x-www-form-urlencoded"}, produces={"application/json"})
    public void addMessage(@RequestBody MultiValueMap paramMap) {
        JsonNode node = null;
        try {
            node = new ObjectMapper().readTree(paramMap.keySet().iterator().next().toString());
            //Przykładowa komenda zapisuje wszystko w kluczu do zmiennej dlatego to tak wygląda
            //curl -X POST localhost:8080/api/message -d '{"email":"jan.kowalski@example.com","title":"Interview","content":"simple text","magic_number":101}'

        } catch (IOException e) {
            e.printStackTrace();
        }


        String insertCql = "insert into messages (id, email, title, content, magic_number) values (" + UUID.randomUUID() + ", '" + node.get("email").textValue() + "', '" + node.get("title").textValue() + "', '" + node.get("content").textValue() + "', " + node.get("magic_number") + ") USING TTL 300";
        cassandraTemplate.getCqlOperations().execute(insertCql);
    }

    @RequestMapping(value="/send", method=RequestMethod.POST, consumes={"application/x-www-form-urlencoded"}, produces={"application/json"})
    public void send(@RequestBody MultiValueMap paramMap) {
        JsonNode node = null;
        try {
            node = new ObjectMapper().readTree(paramMap.keySet().iterator().next().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Message msg : msgRespository.findAll()) {
            if (msg.getMagic_number() == node.get("magic_number").asInt()) {
                emailService.sendMessage(msg.getEmail(), msg.getTitle(), msg.getContent());
                msgRespository.delete(msg);
            }
        }
    }
}