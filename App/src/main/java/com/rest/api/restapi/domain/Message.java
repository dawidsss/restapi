package com.rest.api.restapi.domain;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.util.UUID;

@Table("messages")
public class Message implements Serializable {

    @PrimaryKey
    private UUID id;
    private String email;
    private String title;
    private String content;
    private int magic_number;

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setMagic_number(int magic_number) {
        this.magic_number = magic_number;
    }

    public int getMagic_number() {
        return magic_number;
    }

    public Message(){
        this.id = UUID.randomUUID();
    }
}
