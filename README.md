Server connecting local cassandra on port 9042



---------------------DOCKER--------------------------

Build command
    $ docker build -t some-app

Start command
    $ docker run --name rest-api --net=host -p 8080:8080 some-app

App end points:

    - GET /api/message        returns message list
        $ curl -X GET localhost:8080/api/message

    - POST /api/message        adds message to database
        $ curl -X POST localhost:8080/api/message -d '{"email":"jan.kowalski@example.com","title":"Interview","content":"simple text","magic_number":101}'

    - POST /api/send           sends messages
        $ curl -X POST localhost:8080/api/send -d '{"magic_number":101}'


